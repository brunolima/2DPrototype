﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
  public float speed;

  private Rigidbody2D myRB;

	void Awake () {
    this.myRB = this.GetComponent<Rigidbody2D>();
    Vector2 direction = Vector2.right * this.speed;
    if(this.transform.localRotation.z > 0){
      direction = direction * -1;
    } 
    this.myRB.AddForce(direction, ForceMode2D.Impulse);

	
	}
	
	void Update () {
	
	}

  public void RemoveForce(){
    this.myRB.velocity = Vector2.zero;
  }
}
