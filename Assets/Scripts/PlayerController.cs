﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  
  [Header("Movement")]
  public float maxSpeed;
  public bool facingRight;
  [Header("Jumping")]
  public float jumpHeight;
  public Transform groundCheck;
  public LayerMask groundLayer;
  private bool grounded = false;
  private float groundCheckRadius = 0.2f;
  [Header("Shooting")]
  public Transform gunTip;
  public GameObject bullet;
  public float fireRate = 0.5f;
  private float nextFire = 0f;

  private Rigidbody2D myRB;
  private Animator myAnim;

	void Start () {
    this.myRB = this.GetComponent<Rigidbody2D>();
    this.myAnim = this.GetComponent<Animator>();
	}

  void Update() {
    if(this.grounded && Input.GetAxis("Jump") > 0){
      this.grounded = false;
      this.myAnim.SetBool("isGrounded", this.grounded);
      this.myRB.AddForce(new Vector2(0f, this.jumpHeight));

    }

    //Shooting
    if(Input.GetAxisRaw("Fire1") > 0) FireRocket();
  }
	
	void FixedUpdate () {
    //Check if we are grounded
    this.grounded = Physics2D.OverlapCircle(this.groundCheck.position, this.groundCheckRadius, this.groundLayer);
    this.myAnim.SetBool("isGrounded", this.grounded);
    this.myAnim.SetFloat("verticalSpeed", this.myRB.velocity.y);

    float move = Input.GetAxis("Horizontal");
    this.myAnim.SetFloat("speed", Mathf.Abs(move));
    this.myRB.velocity = new Vector2(move * this.maxSpeed, this.myRB.velocity.y);
    if((move > 0 && !facingRight) || (move < 0 && facingRight)){
      this.Flip();
    }
	}

  void Flip(){
    facingRight = !facingRight;
    Vector3 scale = this.transform.localScale;
    scale.x *= -1;
    this.transform.localScale = scale;

  }

  void FireRocket(){
    if(Time.time > this.nextFire){
      this.nextFire = Time.time + this.fireRate;
      if(this.facingRight){
        this.Instantiate(this.bullet, this.gunTip.position, Quaternion.Euler (new Vector3(0,0,0)));
      } else {
        this.Instantiate(this.bullet, this.gunTip.position, Quaternion.Euler (new Vector3(0,0,180f)));
      }
    }
  }
}
