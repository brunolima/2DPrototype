﻿using UnityEngine;
using System.Collections;

public class rocketHit : MonoBehaviour {

  public float weaponDamage;
  public GameObject explosionEffect;

  private ProjectileController myPC;

	void Awake () {
    this.myPC = this.GetComponentInParent<ProjectileController>();
	}
	
	void Update () {
	
	}

  void OnTriggerEnter2D(Collider2D other){
    this.Collide(other);
  }

  void OnTriggerStay2D(Collider2D other){
    this.Collide(other);
  }

  private void Collide(Collider2D other){
    if(other.gameObject.layer == LayerMask.NameToLayer("Shootable")){
      this.myPC.RemoveForce();
      this.Instantiate(explosionEffect, this.transform.position, this.transform.rotation);
      this.Destroy(this.gameObject);
    }
  }


}
