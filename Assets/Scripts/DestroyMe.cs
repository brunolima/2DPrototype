﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour {
  public float timeToLive;

	void Awake () {
    this.Destroy(this.gameObject, this.timeToLive);
	
	}
}
